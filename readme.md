# README #

Takes screenshots to get the colors on your screen, then sends them to a blinkstrip to ambient lighting.

## Pre-Requisites

ImageMagick must be installed. Expects to find at least `import` and `convert` commands to be available for the main program.

## Commands

`node bin/run.js` - runs the program until terminated by Ctrl-C/SIGTERM
`node bin/test-output.js` - creates colors once in an image instead of using the lights, for debugging.

## Configuration

Copy `.env.example` to `.env` and modify to your needs.

### NUM_LEDS=32

The number of LEDs you have. The more the better.

### SCREENSHOT_WIDTH=40

How large the screenshot should be. On multi-screen setups it can only take all screens. For my 2x 1920*1080 I found that the minimum viable resolution to get actual colors from it is 40x20. The aspect ratio doesn't have to be correct, the height can be larger than it actually is.

### SCREENSHOT_HEIGHT=10

See above.


### MIN_REFRESH_MS=300

The minimum refresh rate in milliseconds.

### MAX_REFRESH_MS=10000

If the colors don't change, the refresh rate will slow down by 1000 each tick until it reaches this value.

### MIN_SATURATION=50

Settings a minimum saturation for each color is recommended. Must be a value between 1 and 100.

### MIN_LUMINOSITY=50

Setting a minimum luminosity value for each color. Must be a value between 1 and 100.

### SCREEN_PARTS_X="0,10,20,30,40"

Splices the screen into parts on these x-pixels. Examples below.

### SCREEN_PARTS_Y="0,5,10"

Splices the screen into parts on these y-pixels. Examples below.

## Screen splicing explained

If the screenshot width/height is set to 40/20, the screenshot pixels are 0-39x, and 0-19y.
Setting the SCREEN_PARTS to the default value as seen above creates 8 sections.

 - 0-9x / 0-4y
 - 10-19x / 0-4y
 - 20-29x / 0-4y
 - 30-39x / 0-4y
 - 0-9x / 5-9y
 - 10-19x / 5-9y
 - 20-29x / 5-9y
 - 30-39x / 5-9y

In each of these sections the colors are averaged out, modified for min luminosity and saturation, and then sent to the light strip.
By setting these values you can define which screen to use, or how many sections to generate.
To use only the right screen: X=20,30,40 Y=0,5,10 (creates 4 sections)
To create vertical slices: X=0,8,16,24,32,40 Y=0,10 (creates 5 sections)
