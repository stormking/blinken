const  { exec, spawn } = require('child_process');
const chalk = require('chalk');
const logUpdate = require('log-update');

function getScreen(w, h) {
	return new Promise(function(resolve, reject) {
		exec(`import -window root -resize ${w}x${h} jpg:- | convert jpg:- txt:-`, {}, (err, stdout) => {	//
			if (err) return reject(err);
			let res = new Uint8Array(w*h*3);
			let i = 0;
			// console.log(stdout);
			stdout.split('\n').forEach((line) => {
				// console.log(i, line);
				let parts = line.split(' ');
				if (parts.length < 2) return;
				let rgb = parts[1].replace('(','').replace(')','').split(',');
				if (rgb.length !== 3) return;
				// console.log(i, rgb);
				rgb.forEach(x => {
					let c = ~~(parseInt(x));
					res[i++] = c;
				})
			});
			resolve(res);
		})
	});
}

function displayInTmpFile(rgb) {
	const blockSize = 160;
	const multi = 256;
	let sub = spawn('display', ['txt:-'], {
	});
	// process.stdout.on('data', s => console.log(s));
	// process.stderr.on('data', s => console.log(s));
	sub.stdin.write(`# ImageMagick pixel enumeration: ${blockSize*rgb.length},${blockSize},65535,srgb`);
	function addBlock(c, sx, sy) {
		for (let y = sy; y < sy + blockSize; y++) {
			for (let x = sx; x < sx + blockSize; x++) {
				sub.stdin.write(`\n${x},${y}: (${c[0]*multi},${c[1]*multi},${c[2]*multi}) #000000 srgb(${c.join(',')})`);
			}
		}
	}
	rgb.forEach((c, i) => addBlock(c, i*blockSize, 0));
	sub.stdin.end();
	process.once('SIGTERM',() => {
		sub.kill();
	});
}
const upperHalf = '▀';
const fullBlock = '█';
const lowerHalf = '▄';
function displayInCli(rgb, repeated) {
	let res = '';
	const multi = 1; //256;
	rgb.forEach((c) => {
		res += chalk.rgb(c[0]*multi, c[1]*multi, c[2]*multi)(fullBlock);
	});
	if (repeated) {
		logUpdate(res)
	} else {
		console.log(res)
	}
}

module.exports = {
	getScreen,
	displayInTmpFile,
	displayInCli
}
