const convert = require('color-convert');
const { minLuminosity, minSaturation } = require('./env');

function getAverage(arr) {
	let ar = 0, ag = 0, ab = 0, n = 0;
	for (let i=0, ii=arr.length; i<ii; i+=3) {
		ar += arr[i];
		ag += arr[i+1];
		ab += arr[i+2];
		n += 1;
	}
	let r = Math.round(ar/n);
	let g = Math.round(ag/n);
	let b = Math.round(ab/n);
	return [r, g, b];
}

function sliceRegion(input, sx, sy, ex, ey, w) {
	let dx = ex - sx;
	let dy = ey - sy;
	let out = new Uint8Array(dx * dy * 3);
	for (let y = sy; y < ey; y++) {
		for (let x = sx; x < ex; x++) {
			let i = (y * w + x) * 3;
			let o = ((y-sy) * dx + (x-sx)) * 3;
			// console.log('-> ', x, y, i, o);
			out[o] = input[i];
			out[o+1] = input[i+1];
			out[o+2] = input[i+2];
		}
	}
	return out;
}

function setMaxLumin(rgb) {
	let hsl = convert.rgb.hsl(rgb);
	// console.log({ hsl });
	hsl[2] = Math.max(hsl[2], minLuminosity);
	hsl[1] = Math.max(hsl[1], minSaturation);
	return convert.hsl.rgb(hsl);
}

function getLedRanges(all, parts) {
	let per = all / parts;
	let f = 0;
	let overflow = 0;
	let res = [];
	for (let i=0; i<parts; i++) {
		let current = overflow + per + 0.00000001;
		overflow = current % 1;
		let num = Math.floor(current);
		res.push([f, f+num]);
		f = f+num;
	}
	return res;
}

module.exports = {
	getAverage,
	sliceRegion,
	setMaxLumin,
	getLedRanges
}
