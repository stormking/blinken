const { getScreen, displayInCli } = require('../src/screen');
const { getAverage, sliceRegion, setMaxLumin, getLedRanges } = require('./util');
const { minRefreshTime, maxRefreshTime, screenshotWidth, screenshotHeight, numLeds, screenPartsX, screenPartsY } = require('./env');

const Blinken = require('../src/blinken');

const led = new Blinken();

let idleTimes = 0;
let lastValue = 0;
let closed = false;

let ranges = getLedRanges(numLeds, (screenPartsX.length-1)*(screenPartsY.length-1));
// console.log(ranges);

function splitGrid(res) {
	let q = [];
	for (let x=0; x<screenPartsX.length-1; x++) {
		for (let y=0; y<screenPartsY.length-1; y++) {
			q.push(sliceRegion(res, screenPartsX[x], screenPartsY[y], screenPartsX[x+1]-1, screenPartsY[y+1]-1, screenshotWidth));
		}
	}
	return q;
}

async function runOnce() {
	getScreen(screenshotWidth, screenshotHeight).then(res => {
		let q = splitGrid(res);
		q.map((rgb) => getAverage(rgb))
			.map(c => setMaxLumin(c))
			.forEach((c, i) => led.setColorRange(c[0], c[1], c[2], ranges[i][0], ranges[i][1], 1000));
	});
}

function update() {
	if (closed) return;
	getScreen(screenshotWidth, screenshotHeight).then(res => {
		let thisValue = 0;
		let q = splitGrid(res);
		// console.log(res, q);
		q = q.map((rgb) => getAverage(rgb))
			.map(c => setMaxLumin(c))
			.map(c => {
				thisValue += c.reduce((c, v) => c + v, 0);
				return c;
			});
		q.forEach((c, i) => led.setColorRange(c[0], c[1], c[2], ranges[i][0], ranges[i][1], minRefreshTime));
		displayInCli(q, true)
		let waitTime;
		if (thisValue === lastValue) {
			idleTimes += 1;
			waitTime = Math.min( maxRefreshTime, idleTimes * 1000 );
		} else {
			idleTimes = 0;
			waitTime = minRefreshTime;
			lastValue = thisValue;
		}
		// console.log(thisValue, idleTimes, waitTime);
		setTimeout(update, waitTime);
	}).catch(e => {
		console.log(e);
		process.exit(5);
	});
}

function hookSigterm() {
	const close = () => {
		closed = true;
		led.setColorAll(0, 0, 0);
	};
	process.on('SIGTERM', close);
	process.on('SIGINT', close);
}

module.exports = {
	splitGrid,
	update,
	hookSigterm,
	runOnce
}
