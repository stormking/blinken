const blinkstick = require('blinkstick');
const { numLeds } = require('./env');

class Blinken {

	constructor() {
		this.led = blinkstick.findFirst();
		this.led.setMode(2);
	}

	setColorAll(r, g, b) {
		for (let i=0; i < numLeds; i++) {
			this.led.morph(r, g, b, {
				channel: 0,
				index: i,
				duration: 1000,
				steps: 1
			});
		}
	}

	setColor(r, g, b, i) {
		this.led.morph(r, g, b, {
			channel: 0,
			index: i,
			duration: 1000,
			steps: 1
		});
	}

	setColorRange(r, g, b, f, t, d) {
		// console.log('setColorRange', { r, g, b, f, t, d });
		for (let i=f; i<t; i++) {
			this.led.morph(r, g, b, {
				channel: 0,
				index: i,
				duration: d,
				steps: 1
			});
		}
	}

}

module.exports = Blinken;
