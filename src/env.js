const dotEnv = require('dotenv');
const { parsed: config } = dotEnv.config({ path: `${__dirname}/../.env` });

const values = {
	numLeds: config.NUM_LEDS ? parseInt(config.NUM_LEDS) : 32,
	screenshotWidth: config.SCREENSHOT_WIDTH ? parseInt(config.SCREENSHOT_WIDTH) : 32,
	screenshotHeight: config.SCREENSHOT_HEIGHT ? parseInt(config.SCREENSHOT_HEIGHT) : 10,
	minRefreshTime: config.MIN_REFRESH_MS ? parseInt(config.MIN_REFRESH_MS) : 500,
	maxRefreshTime: config.MAX_REFRESH_MS ? parseInt(config.MAX_REFRESH_MS) : 10000,
	minSaturation: config.MIN_SATURATION ? parseInt(config.MIN_SATURATION) : 50,
	minLuminosity: config.MIN_LUMINOSITY ? parseInt(config.MIN_LUMINOSITY) : 50,
	screenPartsX: config.SCREEN_PARTS_X ? config.SCREEN_PARTS_X.split(',').map(x => parseInt(x)) : [],
	screenPartsY: config.SCREEN_PARTS_Y ? config.SCREEN_PARTS_Y.split(',').map(x => parseInt(x)) : []
};

// console.log('loaded config', values);

module.exports = values;
