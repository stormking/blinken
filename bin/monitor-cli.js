const { getScreen, displayInTmpFile, displayInCli } = require('../src/screen');
const { getAverage, setMaxLumin } = require('../src/util');
const { screenshotWidth, screenshotHeight } = require('../src/env');
const { splitGrid } = require('../src/main');

async function run() {
	let res = await getScreen(screenshotWidth, screenshotHeight)
	let q = splitGrid(res);
	q = q.map((rgb) => getAverage(rgb))
		.map(c => setMaxLumin(c));
	displayInCli(q, true);
	setTimeout(run, 300);
}

run();
