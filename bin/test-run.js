const { getScreen } = require('../src/screen');
const convert = require('color-convert');
const Blinken = require('../src/blinken');
// const repl = require('repl');

const led = new Blinken();

// const replServer = repl.start({
// 	prompt: '> ',
// 	useGlobal: true
// });
// replServer.context.getScreen = getScreen;
// replServer.context.led = led;
// replServer.on('close',() => {
// 	process.emit('SIGTERM');
// });

let w = 32;
let h = 10;

function getAverage(arr) {
	let ar = 0, ag = 0, ab = 0, n = 0;
	for (let i=0, ii=arr.length; i<ii; i+=3) {
		ar += arr[i];
		ag += arr[i+1];
		ab += arr[i+2];
		n += 1;
	}
	let r = Math.round(ar/n);
	let g = Math.round(ag/n);
	let b = Math.round(ab/n);
	return [r, g, b];
}
function sliceRegion(input, sx, sy, ex, ey) {
	let dx = ex - sx;
	let dy = ey - sy;
	let out = new Uint8Array(dx * dy * 3);
	for (let y = sy; y < ey; y++) {
		for (let x = sx; x < ex; x++) {
			let i = (y * w + x) * 3 + 1;
			let o = ((y-sy) * dx + (x-sx)) * 3;
			// console.log('-> ', x, y, i, o);
			out[o] = input[i];
			out[o+1] = input[i+1];
			out[o+2] = input[i+2];
		}
	}
	return out;
}
function setMaxLumin(rgb) {
	let hsl = convert.rgb.hsl(rgb);
	// console.log({ hsl });
	hsl[2] = Math.max(hsl[2], 50);
	hsl[1] = Math.max(hsl[1], 75);
	return convert.hsl.rgb(hsl);
}
function update() {
	getScreen(w, h).then(res => {
		// console.log('~~~~');
		let q = [
			sliceRegion(res, 16, 0, 24, 5),
			sliceRegion(res, 25, 0, 31, 5),
			sliceRegion(res, 16, 6, 24, 9),
			sliceRegion(res, 25, 6, 31, 9)
		]
		q.forEach((rgb, i) => {
			let c = getAverage(rgb);
			c = setMaxLumin(c);
			// console.log(c);
			led.setColorRange(c[0], c[1], c[2], i*8, i*8+7);
		});

		setTimeout(update, 1000);
	});
}

/*
let cw = 0;
for (let i=32*5*3, ii=res.length; i<ii; i+=3) {
	let r = res[i];
	let g = res[i+1];
	let b = res[i+2];
	led.setColor(r, g, b, cw);
	cw += 1;
	if (cw > 31) break;
}
*/

update();
