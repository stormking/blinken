const { getScreen } = require('../src/screen');
const { getAverage, sliceRegion, setMaxLumin } = require('../src/util');
const convert = require('color-convert');

const w = 32;
const h = 10;
const numLeds = 32;

getScreen(w, h).then(res => {
	console.log(res.join(' '));
	let edgesX = [ 0, 8, 16, 24, 32 ];
	let edgesY = [ 0, 5, 10 ];
	let q = [
		sliceRegion(res, edgesX[0], edgesY[0], edgesX[1]-1, edgesY[1]-1, w),
		sliceRegion(res, edgesX[1], edgesY[0], edgesX[2]-1, edgesY[1]-1, w),
		sliceRegion(res, edgesX[0], edgesY[1], edgesX[1]-1, edgesY[2]-1, w),
		sliceRegion(res, edgesX[1], edgesY[1], edgesX[2]-1, edgesY[2]-1, w),

		sliceRegion(res, edgesX[2], edgesY[0], edgesX[3]-1, edgesY[1]-1, w),
		sliceRegion(res, edgesX[3], edgesY[0], edgesX[4]-1, edgesY[1]-1, w),
		sliceRegion(res, edgesX[2], edgesY[1], edgesX[3]-1, edgesY[2]-1, w),
		sliceRegion(res, edgesX[3], edgesY[1], edgesX[4]-1, edgesY[2]-1, w),
	];
	let ledsPerQ = Math.floor(numLeds / q.length);
	console.log(q);
	q.map((rgb) => getAverage(rgb))
		.map(c => setMaxLumin(c))
		.forEach(c => {
			console.log( c, convert.rgb.keyword(c) );
		});

	let f = 0;
	let overflow = 0;
	let setLights = (c) => {
		let current = overflow + ledsPerQ;
		let overflow = current % 1;
		let num = Math.floor(current);
		led.setColorRange(c[0], c[1], c[2], f, f+num, minUpdateTime)
		f = f+num;
	};
	/*
		.map(c => setMaxLumin(c))
		.map(c => {
			thisValue += c.reduce((c, v) => c + v, 0);
			return c;
		})
		.slice(4)
		.forEach((c, i) => led.setColorRange(c[0], c[1], c[2], i*8, (i+1)*8, minUpdateTime));
	*/
});
