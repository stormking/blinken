const { getScreen, display } = require('../src/screen');
const { getAverage, setMaxLumin } = require('../src/util');
const { screenshotWidth, screenshotHeight } = require('../src/env');
const { splitGrid } = require('../src/main');

getScreen(screenshotWidth, screenshotHeight).then(res => {
	let q = splitGrid(res);
	q = q.map((rgb) => getAverage(rgb))
		.map(c => setMaxLumin(c));

	display(q, 40);

});
